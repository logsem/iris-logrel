# This development is moved
This development is now part of the iris-examples repository, see
[https://gitlab.mpi-sws.org/FP/iris-examples/](https://gitlab.mpi-sws.org/FP/iris-examples/). If
you are interested in the legacy version of this development see
branch [legacy](https://bitbucket.org/logsem/iris-logrel/src/legacy/)
of this repository.
